let firstname  = document.getElementById("fname")
let lastname = document.getElementById("lname")
let email = document.getElementById("email")
let pword = document.getElementById("password")
let confirmpassword = document.getElementById("cPassword")
let myDetails = document.getElementById('my-details')

const getInfo = ()  => {

    if(firstname.value == "" || lastname.value == ""  || email.value == ""  || password.value == ""  || confirmpassword.value == "" ) {
        console.log("Please fill in your information");
    } else if (pword.value.length < 8 || pword.value == "") {
        console.log("Password length is less than 8 characters");
    } else if (pword.value != confirmpassword.value){
        console.log ("passwords does not match. Check Password Fields");
    } else {
        console.log("Thanks for logging your information");
    }
    myDetails.innerHTML = `Hi my full name is ${firstname.value} ${lastname.value} and you can reach me through ${email.value}`
}

firstname.addEventListener('keyup', getInfo)
lastname.addEventListener('keyup', getInfo)
email.addEventListener('keyup', getInfo)
pword.addEventListener('keyup', getInfo)
confirmpassword.addEventListener('keyup', getInfo)